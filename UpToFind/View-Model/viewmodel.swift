//
//  viewmodel.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/10/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit

class viewmodel: NSObject, UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    var imagPickUp : UIImagePickerController!
    var imageV : UIImageView!

    func imageAndVideos()-> UIImagePickerController{
           if(imagPickUp == nil){
               imagPickUp = UIImagePickerController()
               imagPickUp.delegate = self
               imagPickUp.allowsEditing = false
           }
           return imagPickUp
       }
       
     
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
           imageV.image = image
           imagPickUp.dismiss(animated: true, completion: { () -> Void in
               // Dismiss
           })

       }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           imagPickUp.dismiss(animated: true, completion: { () -> Void in
               // Dismiss
           })
       }
    
}
