//
//  ForgotpasswordViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/12/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire

class ForgotpasswordViewController: UIViewController {
    @IBOutlet weak var invalid: UILabel!
    
    
    
    @IBOutlet weak var sendresetcodebtn: UIButton!
    
    @IBOutlet weak var forgotpasswordemail: UITextField!
    @IBAction func sendresetcodetapped(_ sender: Any ) {
        UserDefaults.standard.set(forgotpasswordemail.text, forKey: "emailf")
        let url = "https://www.api.uptofind.tn/api/forget-pass" // UPtofind API Link
         guard let Forgotpasswordemail = forgotpasswordemail.text else {return}
        //let headers  = ["email" : forgotpasswordemail ]
        let param: Parameters = ["email" : Forgotpasswordemail]
        print(param)
        //forgotpassword parameters
        Alamofire.request(url, method: .post,parameters: param, headers: nil )
           .validate(statusCode:200..<300)
            .responseJSON { response in
          switch (response.result) {
                          case .success:
                            self.valid(textfiled:  self.forgotpasswordemail, label: self.invalid)
                              print("succ:\(response)")
                            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ResetpasswordViewController") as!ResetpasswordViewController
                            self.navigationController?.pushViewController(vc, animated: true)

                              break
                          case .failure:
                            if response.response?.statusCode ?? 0 > 300 {
                            self.Invalid(textfiled : self.forgotpasswordemail , label: self.invalid)
                            
                              print("eroor:\(Error.self)")
            }
                            else{
                                print ("no respense")
            }
                }}}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invalid.isHidden = true
        forgotpasswordemail.layer.borderWidth = 0.2
        forgotpasswordemail.layer.borderColor = UIColor.gray.cgColor
        sendresetcodebtn.layer.borderWidth = 0.6
        sendresetcodebtn.layer.borderColor = UIColor.purple.cgColor
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        


        // Do any additional setup after loading the view.
    }
    
    func Invalid(textfiled : UITextField , label : UILabel ) {
        textfiled.layer.borderWidth = 0.4
        textfiled.layer.borderColor = UIColor.red.cgColor
        label.isHidden = false
        
    }
    
    func valid(textfiled : UITextField , label : UILabel ) {
        textfiled.layer.borderWidth = 0.4
        textfiled.layer.borderColor = UIColor.black.cgColor
        label.isHidden = true
        
    }


}
