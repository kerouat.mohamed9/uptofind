//
//  extension.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/13/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import Foundation
import UIKit
public extension UITextField{
    func textfieldstyle (){
        self.layer.cornerRadius = 15.0
        self.clipsToBounds = true
        self.layer.borderWidth = 0.4
        self.layer.borderColor = UIColor.black.cgColor
        
    }
}

extension UILabel {
    func stylelabel(){
        self.layer.cornerRadius = self.frame.height/2
    self.clipsToBounds = true 
    self.layer.borderWidth = 0.4
    self.layer.borderColor = UIColor.black.cgColor
    
    
        
    }
}


public extension UITextField {
    func imageview(_ image: UIImage )
{
    let iconView = UIImageView(frame:
                   CGRect(x: 10, y: 5, width: 20, height: 20))
    iconView.image = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    let iconContainerView: UIView = UIView(frame:
                   CGRect(x: 20, y: 0, width: 30, height: 30))
   
    iconView.tintColor = UIColor.red
    iconContainerView.addSubview(iconView)
    
    rightView = iconContainerView
    rightViewMode = .always
    
    }
}
public extension UILabel {
    func seticon(imagename : String , labelcontent : String) {
        // Create Attachment
                          let imageAttachment = NSTextAttachment()
                          imageAttachment.image = UIImage(named:imagename)
                          // Set bound to reposition
                          let imageOffsetY: CGFloat = -3
                          imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 15, height: 15 )
                          // Create string with attachment
                          let attachmentString = NSAttributedString(attachment: imageAttachment)
                          // Initialize mutable string
                          let completeText = NSMutableAttributedString(string: "")
                          // Add image to mutable string
                          completeText.append(attachmentString)
                          // Add your text to mutable string
                          let textAfterIcon = NSAttributedString(string: labelcontent)
                          completeText.append(textAfterIcon)
                          self.textAlignment = .left
                          self.attributedText = completeText
    }
    
    @available(iOS 13.0, *)
    func setsystemicon(systemimagename : String , labelcontent : String) {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(systemName: systemimagename ) 
        // Set bound to reposition
        let imageOffsetY: CGFloat = 0
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 15, height: 15 )
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        // Add image to mutable string
        completeText.append(attachmentString)
        // Add your text to mutable string
        let textAfterIcon = NSAttributedString(string: labelcontent)
        completeText.append(textAfterIcon)
        self.textAlignment = .center
        self.attributedText = completeText
        
    }
}


public extension UITextField {
func setIcon(_ image: UIImage) {
   let iconView = UIImageView(frame:
                  CGRect(x: 10, y: 5, width: 20, height: 20))
   iconView.image = image
   let iconContainerView: UIView = UIView(frame:
                  CGRect(x: 20, y: 0, width: 30, height: 30))
   iconContainerView.addSubview(iconView)
   leftView = iconContainerView
   leftViewMode = .always
    }
   
}

public extension UIButton {
    func buttonstyle (){
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        self.layer.borderWidth = 0.4
        self.layer.borderColor = UIColor.black.cgColor
        
    }
}

public extension UITextField{
func Invalid(textfiled : UITextField , label : UILabel ) {
    textfiled.layer.borderWidth = 0.4
    textfiled.layer.borderColor = UIColor.red.cgColor
    label.isHidden = false
    
    }}
    
    public extension UITextField{
    
    func valid(textfiled : UITextField , label : UILabel ) {
        textfiled.layer.borderWidth = 0.4
        textfiled.layer.borderColor = UIColor.black.cgColor
        label.isHidden = true
        
        }}

extension UINavigationController {
/**
 It removes all view controllers from navigation controller then set the new root view controller and it pops.
 
 - parameter vc: root view controller to set a new
 */
func initRootViewController(vc: UIViewController) {
    self.viewControllers.removeAll()
    self.pushViewController(vc, animated: false)
    self.popToRootViewController(animated: false)
    }}

var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
extension UIViewController{


        /*
            Show customized activity indicator,
            actually add activity indicator to passing view
        
            @param uiView - add activity indicator to this view
        */
        func showActivityIndicator(uiView: UIView) {
            container.frame = uiView.frame
            container.center = uiView.center
            container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.1)
        
            loadingView.frame =  CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 80, height: 80))

            loadingView.center = uiView.center
            loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x1B0E54, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
        
            activityIndicator.frame =  CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40, height: 40))
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.center = CGPoint(x:loadingView.frame.size.width / 2,y: loadingView.frame.size.height / 2);

            loadingView.addSubview(activityIndicator)
            container.addSubview(loadingView)
            uiView.addSubview(container)
            activityIndicator.startAnimating()
        }

        /*
            Hide activity indicator
            Actually remove activity indicator from its super view
        
            @param uiView - remove activity indicator from this view
        */
        func hideActivityIndicator(uiView: UIView) {
            activityIndicator.stopAnimating()
            container.removeFromSuperview()
        }

        /*
            Define UIColor from hex value
            
            @param rgbValue - hex color value
            @param alpha - transparency level
        */
        func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
            let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
            let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
            let blue = CGFloat(rgbValue & 0xFF)/256.0
            return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
        }
        
   

}
