//
//  TableViewcell.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/24/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
 var frame = CGRect.zero
class TableViewcell: UITableViewCell , UIScrollViewDelegate {

   
    @IBOutlet weak var pagecontroller: UIPageControl!
    
    @IBOutlet weak var scrview: UIScrollView!
    
    @IBOutlet weak var userphoto: UIImageView!
    @IBOutlet weak var numberofcommentslabel: UILabel!
    @IBOutlet weak var numberofweekslabel: UILabel!
    @IBOutlet weak var objectnamelabel: UILabel!
    @IBOutlet weak var adresslabel: UILabel!
    @IBOutlet weak var firstnamelabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userphoto.layer.cornerRadius = userphoto.frame.height / 2
        userphoto.clipsToBounds = true
       scrview.delegate = self



    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrview.contentOffset.x / scrview.frame.size.width
        pagecontroller.currentPage = Int(pageNumber)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
