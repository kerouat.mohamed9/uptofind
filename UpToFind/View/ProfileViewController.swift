//
//  ProfileViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/18/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire
class ProfileViewController: UIViewController {
    @IBOutlet weak var firstname: UILabel!
    @IBOutlet weak var lastname: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var profilimage: UIImageView!
    var url = "https://www.api.uptofind.tn/api/profile"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstname.stylelabel()
        lastname.stylelabel()
        email.stylelabel()
        phone.stylelabel()
        adress.stylelabel()
        self.firstname.text = UserDefaults.standard.string(forKey: "Firstname")
        self.lastname.text = UserDefaults.standard.string(forKey: "Lastname")
        self.email.text = UserDefaults.standard.string(forKey: "Email")
        self.phone.text = UserDefaults.standard.string(forKey: "Phone")
        self.adress.text = UserDefaults.standard.string(forKey: "Adress")

    }
//        let token = UserDefaults.standard.string(forKey: "token" ) ?? ""
//        let param : Parameters = ["token" : token ]
        
//        Alamofire.request(url, method: .get,parameters: param ).validate(statusCode: 200..<300).responseJSON { response in
//
//                                switch (response.result) {
//                                 case .success:
////                                    if let responsevalue = response.result.value as! [String:Any]? {
////                                        if let userdata = responsevalue["data"] as? [String : AnyObject] {
////                                            let Firstname = userdata["firstname"] as? String
////                                            let Lastname = userdata["lastname"]as? String
////                                            let Email = userdata ["email"]as? String
////                                            let Phone = userdata["phone"]as? String
////                                            let Adress = userdata["address"]as? String
////                                            let image = userdata["image"] as? UIImage
//
////                                       print(response)     self.profilimage.image = UserDefaults.standard.string(forKey: "image")
//
//
//
//
//
//
//                                case .failure:
//                                    print(Error.self)
//                                    }}}

}

class Aboutviewcontroller: UIViewController {
    
}
