//
//  SideMenuTableViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/27/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import SideMenu
import UIKit
class SideMenuTableViewController: UITableViewController {
    @IBOutlet weak var sidemenunamelabel: UILabel!
    @IBOutlet weak var Homebutton: UIButton!
    @IBOutlet weak var Addobject: UIButton!
    @IBOutlet weak var Myposts : UIButton!
    @IBOutlet weak var myfavouritepasts: UIButton!
    @IBOutlet weak var profile: UIButton!
    @IBOutlet weak var Setting: UIButton!

    @IBOutlet weak var About: UIButton!


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh cell blur effect in case it changed
        tableView.reloadData()
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

        guard let menu = navigationController as? SideMenuNavigationController, menu.blurEffectStyle == nil else {
            return
        }
        
        // Set up a cool background image for demo purposes
//        let imageView = UIImageView(image: #imageLiteral(resourceName: "saturn"))
//        imageView.contentMode = .scaleAspectFit
//        imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
//        tableView.backgroundView = imageView
    }

        override func viewDidLoad() {
            super.viewDidLoad()
            sidemenunamelabel.text = "K.Mohamed"
            profile.addTarget(self, action:#selector(self.profilebuttontapped), for: .touchUpInside)
            About.addTarget(self, action: #selector(self.Aboutbuttontaped), for: .touchUpInside)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
           }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell

        if let menu = navigationController as? SideMenuNavigationController {
            cell.blurEffectStyle = menu.blurEffectStyle
        }
        
        return cell
    }
    
    @IBAction func profilebuttontapped(_Sender : Any) {
   let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as!ProfileViewController
   self.navigationController?.pushViewController(vc, animated: true)
    dismiss(animated: true, completion: nil)

//        self.present(vc, animated: true, completion: nil)
     }
    @IBAction func Aboutbuttontaped(_Sender : Any) {

       let vc =  self.storyboard?.instantiateViewController(withIdentifier: "Aboutviewcontroller") as!Aboutviewcontroller
       self.navigationController?.pushViewController(vc, animated: true)
        dismiss(animated: true, completion: nil)

    //        self.present(vc, animated: true, completion: nil)
         }
}
