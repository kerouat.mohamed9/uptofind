//
//  SideMenuViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/14/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var viewName: UIButton!

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        // Do any additional setup after loading the view.
    }
    

   override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       // Hide the Navigation Bar
       self.navigationController?.setNavigationBarHidden(true, animated: animated)
   }

  

}
