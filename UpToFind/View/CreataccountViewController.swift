//
//  CreataccountViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/10/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Photos


class CreataccountViewController: UIViewController ,UIImagePickerControllerDelegate , UINavigationControllerDelegate  {
    func passdata(testemail: Bool , testpassword: Bool) {
        
    }
    
    @IBAction func backtologin(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    
    @IBOutlet weak var signupbtn: UIButton!
    
    @IBOutlet weak var emailtextfiled: UITextField!
    @IBOutlet weak var adresstextfiled: UITextField!
    @IBOutlet weak var phonetextfiled: UITextField!
    @IBOutlet weak var passwordtextfiled: UITextField!
    @IBOutlet weak var firstnametextfiled: UITextField!
    @IBOutlet weak var lastnametextfiled: UITextField!
    @IBOutlet weak var button: UIButton!
    
    var imagPickUp : UIImagePickerController!
    var imageV : UIImageView!
    var viewmodel = AddaccountViewModelViewController()
    fileprivate var image : UIImage = (UIImage(named: "icon-user")!)
    
   
    
override func viewDidLoad() {
super.viewDidLoad()
    signupbtn.layer.cornerRadius = 15.0
    signupbtn.backgroundColor = .purple
    
    firstnametextfiled.setIcon(UIImage(named: "icon-user")!)
    lastnametextfiled.setIcon(UIImage(named: "icon-user")!)
    passwordtextfiled.setIcon(UIImage(named: "icon-user")!)
    phonetextfiled.setIcon(UIImage(named: "icon-user")!)        //text field left view icon
    emailtextfiled.setIcon(UIImage(named: "icon-user")!)
    adresstextfiled.setIcon(UIImage(named: "icon-user")!)
    
    firstnametextfiled.textfieldstyle()
    lastnametextfiled.textfieldstyle()
    passwordtextfiled.textfieldstyle()   // text field round style
    phonetextfiled.textfieldstyle()
    emailtextfiled.textfieldstyle()
    adresstextfiled.textfieldstyle()
    
    
    let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white

        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    
        imagPickUp = self.imageAndVideos()

        //let button1 = UIButton(frame: CGRect(x: 100, y: 200, width: 50, height: 50))
        //button = button1
        button.center = view.center
        button.backgroundColor = .white
        button.layer.cornerRadius = button.frame.height / 2
        button.clipsToBounds = true
        button.tintColor = .purple
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.view.addSubview(button)


        imageV = UIImageView(frame: CGRect(x: 100, y: 200, width: 150, height: 150))
        imageV.layer.borderWidth = 0.4
        imageV.layer.borderColor = UIColor.black.cgColor
        imageV.layer.cornerRadius = imageV.frame.height / 2
        imageV.clipsToBounds = true
        view.addSubview(imageV)
    }
    
   

    @IBAction func signuptapped(_ sender: Any) {

        let data = ["firstname": firstnametextfiled.text ,"lastname":lastnametextfiled.text ,"email": emailtextfiled.text , "password": passwordtextfiled.text , "phone" : phonetextfiled.text ,"adresse": adresstextfiled.text ] as! [String : String]
      
        
        viewmodel.passdata(data: data , image : image )
    }

       @objc func buttonClicked() {
    let ActionSheet = UIAlertController(title: nil, message: "Select Photo", preferredStyle: .actionSheet)

              let cameraPhoto = UIAlertAction(title: "Camera", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){

                      self.imagPickUp.mediaTypes = ["public.image"]
                      self.imagPickUp.sourceType = UIImagePickerController.SourceType.camera;
                      self.present(self.imagPickUp, animated: true, completion: nil)
                  }
                  else{
                      UIAlertController(title: "iOSDevCenter", message: "No Camera available.", preferredStyle: .alert).show(self, sender: nil);
                  }

              })

              let PhotoLibrary = UIAlertAction(title: "Photo Library", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                      self.imagPickUp.mediaTypes = ["public.image"]
                      self.imagPickUp.sourceType = UIImagePickerController.SourceType.photoLibrary;
                      self.present(self.imagPickUp, animated: true, completion: nil)
                  }

              })

              let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                  (alert: UIAlertAction) -> Void in

              })

              ActionSheet.addAction(cameraPhoto)
              ActionSheet.addAction(PhotoLibrary)
              ActionSheet.addAction(cancelAction)


              if UIDevice.current.userInterfaceIdiom == .pad{
                  let presentC : UIPopoverPresentationController  = ActionSheet.popoverPresentationController!
                  presentC.sourceView = self.view
                  presentC.sourceRect = self.view.bounds
                  self.present(ActionSheet, animated: true, completion: nil)
              }
              else{
                  self.present(ActionSheet, animated: true, completion: nil)
              }
          }
       func imageAndVideos()-> UIImagePickerController{
           if(imagPickUp == nil){
               imagPickUp = UIImagePickerController()
               imagPickUp.delegate = self
               imagPickUp.allowsEditing = false
           }
           return imagPickUp
       }
       
     
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
            let assetResources = PHAssetResource.assetResources(for: asset)

            print(assetResources.first!.originalFilename)
        
            self.image = image!

        }
           imageV.image = image
    
           imagPickUp.dismiss(animated: true, completion: { () -> Void in
               // Dismiss
           })

       }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           imagPickUp.dismiss(animated: true, completion: { () -> Void in
               // Dismiss
           })
       }
    

}
