//
//  ResetpasswordViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/20/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire
class ResetpasswordViewController: UIViewController {
    @IBOutlet weak var confirmresetcodebtn: UIButton!
    @IBOutlet weak var resetcodefield: UITextField!
    @IBOutlet weak var invalidcodelabel: UILabel!
    @IBAction func confirmresetcodetapped(_ sender: Any) {
        let email = UserDefaults.standard.string(forKey: "emailf" ) ?? ""

        let url = "https://www.api.uptofind.tn/api/verifiedCode" // UPtofind API Link
             guard let Resetcodefield = resetcodefield.text else {return}
        let param: Parameters = ["code" : Resetcodefield , "email" : email ]
            print(param)
            //forgotpassword parameters
            Alamofire.request(url, method: .post,parameters: param, headers: nil )
               .validate(statusCode:200..<300)
                .responseJSON { response in
              switch (response.result) {
                              case .success:
                                self.resetcodefield.valid(textfiled: self.resetcodefield, label: self.invalidcodelabel)
                                  print("succ:\(response)")
                                let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ConfirmresetpasswordViewController") as!ConfirmresetpasswordViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                                  break
                              case .failure:
                                if response.response!.statusCode > 300 {
                                    self.resetcodefield.Invalid(textfiled : self.resetcodefield , label: self.invalidcodelabel)
                                
                                  print("eroor:\(Error.self)")
                }}}
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       invalidcodelabel.isHidden = true
        confirmresetcodebtn.layer.borderWidth = 0.6
        confirmresetcodebtn.layer.borderColor = UIColor.purple.cgColor
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
