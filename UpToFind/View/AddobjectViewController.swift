//
//  AddobjectViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 3/19/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit

class AddobjectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
              navigationController?.setNavigationBarHidden(true, animated: animated)

          }


}


class IlostobjectViewController: UIViewController {

    @IBOutlet weak var selectdate: UIButton!
    @IBOutlet weak var category: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
              navigationController?.setNavigationBarHidden(true, animated: animated)

          }

    @IBAction func categorytapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Books", style: .default) { _ in
            self.category.setTitle("Books", for: .normal)
        })

        alert.addAction(UIAlertAction(title: "Pets", style: .default) { _ in
            self.category.setTitle("Pets", for: .normal)

                    })
        alert.addAction(UIAlertAction(title: "Electronics & computers ", style: .default) { _ in
                  self.category.setTitle("Electronics & computers", for: .normal)

                          })
        alert.addAction(UIAlertAction(title: "clothings", style: .default) { _ in
                  self.category.setTitle("clothings", for: .normal)

                          })
        alert.addAction(UIAlertAction(title: "cars & vehicles", style: .default) { _ in
                        self.category.setTitle("cars & vehicles", for: .normal)

                                })
        alert.addAction(UIAlertAction(title: "Personal objects", style: .default) { _ in
                        self.category.setTitle("Personal objects", for: .normal)

                                })
        alert.addAction(UIAlertAction(title: "People", style: .default) { _ in
        self.category.setTitle("People", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Jewelry", style: .default) { _ in
        self.category.setTitle("Jewelry", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Music instrument", style: .default) { _ in
        self.category.setTitle("Music instrument", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Others", style: .default) { _ in
        self.category.setTitle("Others", for: .normal)

                })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction) -> Void in

        })
        alert.addAction(cancelAction)
        present(alert, animated: true)
    
    }
    @IBAction func selectdatetapped(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = UIDatePicker.Mode.date
       let todayDate = Calendar.current.date(byAdding: .day, value: 0 , to: Date())
       myDatePicker.maximumDate = todayDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let somethingAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction) -> Void in

            let selectedDate = dateFormatter.string(from: myDatePicker.date)
            self.selectdate.setTitle(selectedDate, for: .normal)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
        
    }
    
}
class IfoundobjectViewController: UIViewController {

    @IBOutlet weak var selectdate: UIButton!
    @IBOutlet weak var category: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        selectdate.addTarget(self, action:#selector(self.selectdatetapped), for: .touchUpInside)
        category.addTarget(self, action:#selector(self.categorytapped), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
              navigationController?.setNavigationBarHidden(true, animated: animated)

          }

    @IBAction func categorytapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Books", style: .default) { _ in
            self.category.setTitle("Books", for: .normal)
        })

        alert.addAction(UIAlertAction(title: "Pets", style: .default) { _ in
            self.category.setTitle("Pets", for: .normal)

                    })
        alert.addAction(UIAlertAction(title: "Electronics & computers ", style: .default) { _ in
                  self.category.setTitle("Electronics & computers", for: .normal)

                          })
        alert.addAction(UIAlertAction(title: "clothings", style: .default) { _ in
                  self.category.setTitle("clothings", for: .normal)

                          })
        alert.addAction(UIAlertAction(title: "cars & vehicles", style: .default) { _ in
                        self.category.setTitle("cars & vehicles", for: .normal)

                                })
        alert.addAction(UIAlertAction(title: "Personal objects", style: .default) { _ in
                        self.category.setTitle("Personal objects", for: .normal)

                                })
        alert.addAction(UIAlertAction(title: "People", style: .default) { _ in
        self.category.setTitle("People", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Jewelry", style: .default) { _ in
        self.category.setTitle("Jewelry", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Music instrument", style: .default) { _ in
        self.category.setTitle("Music instrument", for: .normal)

                })
        alert.addAction(UIAlertAction(title: "Others", style: .default) { _ in
        self.category.setTitle("Others", for: .normal)

                })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction) -> Void in

        })
        alert.addAction(cancelAction)
        present(alert, animated: true)
    
    }
    @IBAction func selectdatetapped(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = UIDatePicker.Mode.date
       let todayDate = Calendar.current.date(byAdding: .day, value: 0 , to: Date())
       myDatePicker.maximumDate = todayDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let somethingAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction) -> Void in

            let selectedDate = dateFormatter.string(from: myDatePicker.date)
            self.selectdate.setTitle(selectedDate, for: .normal)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
        
    }
    
}
