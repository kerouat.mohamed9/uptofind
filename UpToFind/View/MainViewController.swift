//
//  MainViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/12/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import CoreData

var URl = URL(string: "http://192.168.1.16:8001/api/getAdsWithInvisible" )
var url = "https://www.api.uptofind.tn/api/"
class MainViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    var frame = CGRect.zero

    var  numberofpost : Int = 1
    var  Post = post(firstname: [], lastname: [], nameobject: [])
    
    
    var profilFirstName : String? = ""
    var profilLastName : String? = ""
    var profilPhoto : String? = ""
    var profileEmail : String? = ""
    var profilePhone : String? = ""
    var profileAdress : String? = ""
    @IBOutlet weak var Tableview: UITableView!
   
    func getads()  {
         let token = UserDefaults.standard.string(forKey: "token" ) ?? ""
        let param : Parameters = ["token" : token ]
       
                

        Alamofire.request(url+"getAdsWithInvisible", method: .get,parameters: param).responseWelcome{ response in
            switch (response.result) {
            case .success:
                
               

             if let welcome = response.result.value {
//                print(welcome.data)
//                print("number of post =  \(welcome.data?.count)")
                if let Numberofpost = welcome.data?.count {
                    self.numberofpost  = Numberofpost}
                
                
                if welcome.data?.count != nil && welcome.data?.count != 0 {
                    for x in 0..<self.numberofpost {
                self.Post.firstname?.append((welcome.data![x].user?.firstname ?? "Error-firstname"))
                self.Post.lastname?.append((welcome.data![x].user?.lastname ?? "Error-lastname"))
                self.Post.profilAdress?.append((welcome.data![x].user?.address ?? "Error-address" ))
                self.Post.profilEmail?.append((welcome.data![x].user?.email ?? "Error-email" ))
                self.Post.profilPhone?.append((welcome.data![x].user?.phone ?? "Error-phone" ))

                self.Post.nameobject?.append((welcome.data![x].nameObj ?? "Error-nameobj"))
                self.Post.adress?.append((welcome.data![x].address ?? "Error-adress"))
                self.Post.numberofcomments?.append((welcome.data![x].comments?.count ?? 0 ))
                self.Post.userphoto?.append((welcome.data![x].user?.image ?? "Error-photo"))
                if welcome.data?[x].adsImages?.count != 0 {
                    
                    if welcome.data?[x].adsImages?.count == 1{
                      for z in 0..<welcome.data![x].adsImages!.count
                    
                    {print(x,z)
                        self.Post.postphoto?.append(([welcome.data![x].adsImages![z].image ?? "Error-postimage"]))
                        }}
                    else {
                        
                    var postPhoto : [String] = []
                     for z in 0...welcome.data![x].adsImages!.count-1
                        {
                            postPhoto.append((welcome.data![x].adsImages![z].image ?? "Error-postimage"))
                        }
                        self.Post.postphoto?.append(postPhoto)
                    }}
//                    else {self.Post.postphoto?.append([""])} //Append default image to keep order of array
//
//
                if welcome.data?[x].comments?.count != 0 {
                for y in 0...welcome.data![x].comments!.count-1 {
                    self.Post.comments?.append((welcome.data![x].comments![y].content ?? "Error-commentcontent"))
                    }}
               

                    }
//                     print(self.Post)
                }else {print("no post available")}
                
                self.Tableview.reloadData()
             }
             break
             case .failure:
                print (response)

                print("get ads \(Error.self)") }}}
       
    
    
 
    @IBOutlet weak var sidemenuview: UIView!
    @IBAction func sidemenutapped(_ sender: Any) {

        // Define the menu
        //let menu = SideMenuNavigationController(rootViewController: self)
        // SideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showActivityIndicator(uiView:self.view)
        Tableview?.rowHeight = 349
        setupSideMenu()
        getads()
        Tableview.delegate = self
        Tableview.dataSource = self


       }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(true, animated: animated)

        }
    private func setupSideMenu() {
          var setting = SideMenuSettings()
             

               setting.statusBarEndAlpha = 0

               setting.presentationStyle = .menuSlideIn
               
               SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
               
               SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
               SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
           
       }

        func do_table_refresh(){

            DispatchQueue.main.async(execute: {
                self.Tableview.reloadData()
                return
            })
        }
   
   



   

    //TableView -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           print("there is  \(Post.firstname!.count) posts ")
        return Post.firstname!.count
           
         }

       

         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewcell", for: indexPath) as! TableViewcell
             if Post.firstname?.count != 0 {
                 if let firstname = Post.firstname?[indexPath.row] {
                    if let lastname = Post.lastname?[indexPath.row]{
                    if let adress = Post.adress?[indexPath.row]{
                    if let numberofcomments = Post.numberofcomments?[indexPath.row]{
                    if let nameobject = Post.nameobject?[indexPath.row]{
                    if let userphoto = Post.userphoto?[indexPath.row]{
                    if let postphoto = (Post.postphoto?[indexPath.row]){
//                    if Post.postphoto?.count != 0 {
//                       let postphoto = (Post.postphoto?[indexPath.row])
//                       let postPhoto = postphoto?[0]
//
//                        if postphoto!.count == 1 {
//
//                        Alamofire.request("https://www.api.uptofind.tn/"+postPhoto!, method: .get)
//                        .validate()
//                        .responseData(completionHandler: { (responseData) in
//                            cell.frame.size = cell.scrollview.frame.size
//
//                            let imageView = UIImageView(frame: cell.frame)
//                            imageView.image = UIImage(data: responseData.data!)
//                            cell.scrollview.addSubview(imageView)
//                        })}
//                        else{
//                            for z in 0...postphoto!.count-1{
//                            let postPhoto = postphoto?[z]
//                            Alamofire.request("https://www.api.uptofind.tn/"+postPhoto!, method: .get)
//                            .validate()
//                            .responseData(completionHandler: { (responseData) in
//                                cell.frame.size = cell.scrollview.frame.size
//
//                                let imageView = UIImageView(frame: cell.frame)
//                                imageView.image = UIImage(data: responseData.data!)
//                                cell.scrollview.addSubview(imageView)
//                            })
//
//                            }
//
//
//                        }
//                    }
                    cell.firstnamelabel.text = firstname+" "+lastname
                    cell.adresslabel.text = adress
                    cell.numberofcommentslabel.text = "Post have \(numberofcomments) comments"
                    cell.objectnamelabel.text = nameobject
                       

                        Alamofire.request("https://www.api.uptofind.tn/"+userphoto, method: .get)
                                .validate()
                                .responseData(completionHandler: { (responseData) in
                                    cell.userphoto.image = UIImage(data: responseData.data!)
                                    
                                })
                        if postphoto.count == 1 {
//                            frame.origin.x = cell.scrview.frame.size.width
//                            frame.size = cell.scrview.frame.size
//                            Alamofire.request("https://www.api.uptofind.tn/"+postphoto[0], method: .get)
//                                           .validate()
//                                    .responseData(completionHandler: { (responseData) in
//                                                cell.frame.size = cell.scrview.frame.size
//
//                                        let imgView = UIImageView(frame: self.frame)
//                                         imgView.image = UIImage(data: responseData.data!)
////                                        cell.scrview.addSubview(imgView)
//                                        cell.scrview.contentSize = CGSize(width: (cell.scrview.frame.size.width * CGFloat(postphoto.count)), height: cell.scrview.frame.size.height)
//                                        cell.scrview.delegate = self
//                                    })
                            
                        }
                        else{
//                            for z in 0...postphoto.count-1{
//                                frame.origin.x = cell.scrview.frame.size.width * CGFloat(z)
//                                frame.size = cell.scrview.frame.size
//
//                                let postPhoto = postphoto[z]
//                                Alamofire.request("https://www.api.uptofind.tn/"+postPhoto, method: .get)
//                            .validate()
//                            .responseData(completionHandler: { (responseData) in
//                                cell.frame.size = cell.scrview.frame.size
//
//                                let imgView = UIImageView(frame: self.frame)
//                                imgView.image = UIImage(data: responseData.data!)
//                                cell.scrview.addSubview(imgView)
//                                cell.scrview.contentSize = CGSize(width: (cell.scrview.frame.size.width * CGFloat(postphoto.count)), height: cell.scrview.frame.size.height)
//                                cell.scrview.delegate = self
//                                                        })
//
//                            }
                            
                        }
                            
                        
                            
                            
                        
                        
                    print(Post)
                        }}}}}}}
                hideActivityIndicator(uiView: self.view)
             }
              else {
                 print("No posts ")
             }


             return cell
         }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        profilFirstName = Post.firstname?[indexPath.row]
        profilLastName = Post.lastname?[indexPath.row]
        profilPhoto = Post.userphoto?[indexPath.row]
        profileEmail = Post.profilEmail?[indexPath.row]
        profileAdress = Post.profilAdress?[indexPath.row]
        profilePhone = Post.profilPhone?[indexPath.row]
        performSegue(withIdentifier: "showProfil" , sender: nil )
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let profile = segue.destination as! userprofileViewController
        profileinfoViewController.firstName = profilFirstName
        profileinfoViewController.lastName = profilLastName
        profile.Photo = profilPhoto
        profileinfoViewController.Phone = profilePhone
        profileinfoViewController.Adress = profileAdress
        profileinfoViewController.Email = profileEmail
        
        
    }
    
    
    struct post {
        var firstname : [String]? = []
        var lastname : [String]? = []
        var nameobject : [String]? = []
        var comments : [String]? = []
        var adress : [String]? = []
        var numberofcomments :[Int]? = []
        var postphoto :[[String]]? = []
        var userphoto :[String]? = []
        var profilEmail : [String]? = []
        var profilPhone : [String]? = []
        var profilAdress : [String]? = []
        
        
    }
    
    
    
    

   }



