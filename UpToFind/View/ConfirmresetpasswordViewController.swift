//
//  ConfirmresetpasswordViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/20/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire
class ConfirmresetpasswordViewController: UIViewController {
    let button1 = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    let button2 = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
var secure1 = true
var secure2 = true
    @IBOutlet weak var resetcodebtn: UIButton!
    @IBOutlet weak var newpassword : UITextField!{
        didSet{
        newpassword.rightView = button1
        newpassword.rightViewMode = .always}
    }
    
    @IBOutlet weak var confirmnewpassword: UITextField!{
        didSet{
        confirmnewpassword.rightView = button2
        confirmnewpassword.rightViewMode = .always}
    }
    @IBOutlet weak var invalidepasswordlabel: UILabel!
    let url = "http://192.168.1.16:8001/api/resetPasswordWithCode" // UPtofind API Link
    

    @IBAction func confirmresettapped(_ sender: Any) {
        let email = UserDefaults.standard.string(forKey: "emailf" ) ?? ""

        let Newpassword = newpassword.text
        let Confirmnewpassword = confirmnewpassword.text
        if Newpassword == Confirmnewpassword {
            let param: Parameters = ["email" : email , "password" : Confirmnewpassword ?? "" ]
            print(param)
            //forgotpassword parameters
            Alamofire.request(url, method: .post,parameters: param, headers: nil )
               .validate(statusCode:200..<300)
                .responseJSON { response in
              switch (response.result) {
                              case .success:
                                self.newpassword.valid(textfiled: self.newpassword, label: self.invalidepasswordlabel)
                                  print("succ:\(response)")
                                  break
                              case .failure:
                                if response.response!.statusCode > 300 {
                                    self.newpassword.Invalid(textfiled : self.newpassword , label: self.invalidepasswordlabel)
                                
                                  print("eroor:\(Error.self)")
                }}}}
        
        else{
            newpassword.Invalid(textfiled: newpassword, label: invalidepasswordlabel)}}
    @objc func button1Clicked() {
    secure1 = !secure1
        updatesecure(btn: button1 , TF:newpassword , secure: secure1 )
     }
    @objc func button2Clicked() {
    secure2 = !secure2
     updatesecure(btn: button2 , TF:confirmnewpassword , secure: secure2 )
     }
    
    
    func updatesecure(btn : UIButton ,TF : UITextField ,secure : Bool) {
       if secure == true{
           
       btn.setImage(UIImage(named: "visibility_off"), for: .normal)
       TF.isSecureTextEntry = true
           
       }
       else {btn.setImage(UIImage(named: "visibillity_on"), for: .normal)
           TF.isSecureTextEntry = false
       }
       }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invalidepasswordlabel.isHidden = true
        resetcodebtn.layer.borderWidth = 0.6
        resetcodebtn.layer.borderColor = UIColor.purple.cgColor
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        //change back button name to ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    button1.addTarget(self, action:#selector(self.button1Clicked), for: .touchUpInside)
    self.view.addSubview(button1)
    button2.addTarget(self, action:#selector(self.button2Clicked), for: .touchUpInside)
    self.view.addSubview(button2)

 }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
}
