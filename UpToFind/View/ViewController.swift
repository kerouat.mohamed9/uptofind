//
//  ViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/4/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
    
    
    
    @IBOutlet weak var remembermebtn: UIButton!
    var viewmodel = ViewControllervm()
    let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    let alert = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    var secure = true
    var rememberme = false
    @IBOutlet weak var loginbtn: UIButton!
    
    @IBOutlet weak var Usernametextfield: UITextField!{
       didSet {
        Usernametextfield.tintColor = UIColor.black
        Usernametextfield.setIcon(UIImage(named: "icon-user")!)
        Usernametextfield.textfieldstyle()

       
        
       }
    }
    
   
    @IBOutlet weak var Passwordtextfield: UITextField!{
        didSet {
        Passwordtextfield.tintColor = UIColor.black
        Passwordtextfield.setIcon(UIImage(named: "icon-user")!)
        Passwordtextfield.rightView = button
        Passwordtextfield.rightViewMode = .always
        Passwordtextfield.textfieldstyle()

       }
}
   
   
    
    @IBAction func forgotpasswordtapped(_ sender: Any) {
           // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ForgotpasswordViewController")
            self.navigationController?.pushViewController(vc, animated: true)
    
        }
    
    @IBAction func creataccountbtntapped(_ sender: Any) {
 
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "CreataccountViewController") as!CreataccountViewController
       self.navigationController?.pushViewController(vc, animated: true)
     
    }
    
    @IBAction func remembermetapped(_ sender: Any) {
        rememberme = !rememberme
        updateremember()
        }
    
func updateremember(){
if rememberme == false
{remembermebtn.setImage(UIImage(named: "icon-user")!, for: .normal)
remembermebtn.tintColor = .purple
}else {
remembermebtn.setImage(UIImage(named: "icon-user")!, for: .normal)
remembermebtn.tintColor = .purple }}
        
    @IBAction func logintapped(_ sender: Any) {
            saveuserdefault()
            viewmodel.sendValue(from: Usernametextfield.text, passwordTextField: Passwordtextfield.text,alert: Usernametextfield ,completion: {
                let vc =  self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as!MainViewController
        self.navigationController?.pushViewController(vc, animated: true)
            })
            
        }
        
    @objc func buttonClicked() {
       secure = !secure
        updatesecure()
        }
    
    
    func updatesecure() {
    if secure == true{
        
    button.setImage(UIImage(named: "visibility_off"), for: .normal)
    Passwordtextfield.isSecureTextEntry = true
        
    }
    else {button.setImage(UIImage(named: "visibillity_on"), for: .normal)
        Passwordtextfield.isSecureTextEntry = false
    }
    }
    
    func saveuserdefault(){
        UserDefaults.standard.set(Usernametextfield.text, forKey: "email")
        UserDefaults.standard.set(Passwordtextfield.text, forKey: "password")
        UserDefaults.standard.set(rememberme, forKey: "rememberme")
        


    }
    func postuserdefault(){
    let email = UserDefaults.standard.string(forKey: "email" ) ?? ""
    let password = UserDefaults.standard.string(forKey: "password") ?? ""
    Usernametextfield.text = email
    Passwordtextfield.text = password
    }
    func rememberuserdefault() ->Bool  {
        let status = UserDefaults.standard.bool(forKey: "rememberme") ?? false
        rememberme = status
        return (status)
    }
        
        
        
    

override func viewDidLoad() {
super.viewDidLoad()

rememberuserdefault()
updatesecure()
if rememberuserdefault(){
postuserdefault()}
    
loginbtn.layer.cornerRadius = 15.0
loginbtn.backgroundColor = .purple
    




    

button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
self.view.addSubview(button)
    
self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
self.navigationController!.navigationBar.shadowImage = UIImage()
self.navigationController!.navigationBar.isTranslucent = true
}
}


