//
//  profileinfoViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 4/2/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class profileinfoViewController: UIViewController {
    @IBOutlet weak var firstname: UIButton!
    
    @IBOutlet weak var lastname: UIButton!
    
    @IBOutlet weak var email: UIButton!
    
    @IBOutlet weak var phone: UIButton!
    
    @IBOutlet weak var adress: UIButton!
    
    static var firstName : String? = ""
    static var lastName : String? = ""
    static var Phone : String? = ""
    static var Email : String? = ""
    static var Adress : String? = ""
    
let usercviewcontorller = userprofileViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let token = UserDefaults.standard.string(forKey: "token" ) ?? ""
//        getprofileinfo(parameters: ["id":2 , "token" : token])
        filllabel()
        firstname.buttonstyle()
        lastname.buttonstyle()
        email.buttonstyle()
        phone.buttonstyle()
        adress.buttonstyle()
        adress.titleLabel?.minimumScaleFactor = 0.5
        adress.titleLabel?.numberOfLines = 1
        adress.titleLabel?.adjustsFontSizeToFitWidth = true
        
    }
    
    
    func filllabel() {
       
        
        self.firstname.setTitle(profileinfoViewController.firstName, for: .normal)
        self.lastname.setTitle(profileinfoViewController.lastName, for: .normal)
        self.email.setTitle(profileinfoViewController.Email, for: .normal)
        self.phone.setTitle(profileinfoViewController.Phone, for: .normal)
        self.adress.setTitle(profileinfoViewController.Adress, for: .normal)    }
       
    

    

}

