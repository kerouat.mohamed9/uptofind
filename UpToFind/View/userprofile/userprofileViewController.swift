//
//  userprofileViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 3/31/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class userprofileViewController: UIViewController {

    @IBOutlet weak var profilephoto: UIImageView!
    @IBOutlet weak var profileinfo: UIView!
    
    @IBOutlet weak var profileannounce: UIView!
    @IBOutlet weak var profilecomment: UIView!
   
    var image : UIImage?
    var Photo : String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilephoto.layer.borderWidth = 0.4
        profilephoto.layer.borderColor = UIColor.black.cgColor
        profilephoto.layer.cornerRadius = profilephoto.frame.height / 2
        profilephoto.clipsToBounds = true
        profileannounce.isHidden = true
        profilecomment.isHidden = true
        getprofileimage() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(animated)
               navigationController?.setNavigationBarHidden(false, animated: animated)

           }
    
    
    @IBAction func segmenttapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0
        { profileinfo.isHidden = false
            profileannounce.isHidden = true
            profilecomment.isHidden = true
            
        }
        if sender.selectedSegmentIndex == 1
        { profileinfo.isHidden = true
          profileannounce.isHidden = false
          profilecomment.isHidden = true
        }
         if sender.selectedSegmentIndex == 2
        {
            profileinfo.isHidden = true
            profileannounce.isHidden = true
            profilecomment.isHidden = false
        }
    }
    
    func getprofileimage() {
        
        Alamofire.request("https://www.api.uptofind.tn/"+Photo!, method: .get)
        .validate()
        .responseData(completionHandler: { (responseData) in
            self.profilephoto.image = UIImage(data: responseData.data!)
            
        })
        
        
        
    }
    
    
    
    
    
    
}
