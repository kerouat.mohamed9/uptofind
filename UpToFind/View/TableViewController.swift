//
//  TableViewController.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/14/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//
import SideMenu
import UIKit

class TableViewController : UITableViewController {
    

        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            // refresh cell blur effect in case it changed
            tableView.reloadData()
            
            guard let menu = navigationController as? SideMenuNavigationController, menu.blurEffectStyle == nil else {
                return
            }
            
            // Set up a cool background image for demo purposes
            let imageView = UIImageView(image: #imageLiteral(resourceName: "saturn"))
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            tableView.backgroundView = imageView
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell

            if let menu = navigationController as? SideMenuNavigationController {
                cell.blurEffectStyle = menu.blurEffectStyle
            }
            
            return cell
        }
        
    }

//    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//         let headerimageview = UIImageView(frame: CGRect(x: 100, y: 200, width: 150, height: 150))
//         let image = UIImage(named: "portrait-homme-blanc-isole_53876-40306")
//         headerimageview.image = image;
//        headerimageview.layer.cornerRadius = 10
//        headerimageview.clipsToBounds = true
//        headerimageview.layer.cornerRadius = headerimageview.frame.height / 2
//        headerimageview.clipsToBounds = true
//
//         header.contentView.addSubview(headerimageview)
//    }
   


//headerimageview.layer.cornerRadius = 10
//headerimageview.clipsToBounds = true
//headerimageview.layer.cornerRadius = headerimageview.frame.height / 2
//headerimageview.clipsToBounds = true
