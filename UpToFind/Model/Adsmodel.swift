// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try Welcome(json)

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseWelcome { response in
//     if let welcome = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - Welcome
struct Welcome: Codable {
    let currentPage: Int?
    let data: [Datum]?
    let firstPageURL: String?
    let from, lastPage: Int?
    let lastPageURL, nextPageURL, path: String?
    let perPage: Int?
    let prevPageURL: JSONNull?
    let to, total: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage
        case data
        case firstPageURL
        case from
        case lastPage
        case lastPageURL
        case nextPageURL
        case path
        case perPage
        case prevPageURL
        case to, total
    }
}

// MARK: Welcome convenience initializers and mutators

extension Welcome {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Welcome.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        currentPage: Int?? = nil,
        data: [Datum]?? = nil,
        firstPageURL: String?? = nil,
        from: Int?? = nil,
        lastPage: Int?? = nil,
        lastPageURL: String?? = nil,
        nextPageURL: String?? = nil,
        path: String?? = nil,
        perPage: Int?? = nil,
        prevPageURL: JSONNull?? = nil,
        to: Int?? = nil,
        total: Int?? = nil
    ) -> Welcome {
        return Welcome(
            currentPage: currentPage ?? self.currentPage,
            data: data ?? self.data,
            firstPageURL: firstPageURL ?? self.firstPageURL,
            from: from ?? self.from,
            lastPage: lastPage ?? self.lastPage,
            lastPageURL: lastPageURL ?? self.lastPageURL,
            nextPageURL: nextPageURL ?? self.nextPageURL,
            path: path ?? self.path,
            perPage: perPage ?? self.perPage,
            prevPageURL: prevPageURL ?? self.prevPageURL,
            to: to ?? self.to,
            total: total ?? self.total
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
struct Datum: Codable {
    let id: Int?
    let nameObj, datumDescription, dateAnnonce, colorID: String?
    let colorName, longitude, latitude, address: String?
    let userID, categoryID: String?
    let boostID: JSONNull?
    let reward: String?
    let state, favoris, enabled, acceptedSignal: String?
    let itemFound: String?
    let deletedAt: JSONNull?
    let createdAt, updatedAt: String?
    let comments: [Comment]?
    let user: User?
    let category: Category?
    let adsImages: [AdsImage]?
    let color: Color?

    enum CodingKeys: String, CodingKey {
        case id
        case nameObj = "name_obj"
        case datumDescription
        case dateAnnonce
        case colorID = "color_id"
        case colorName, longitude, latitude, address
        case userID = "user_id"
        case categoryID = "category_id"
        case boostID
        case reward, state, favoris, enabled
        case acceptedSignal
        case itemFound = "item_found"
        case deletedAt
        case createdAt  =  "created_at"
        case updatedAt =  "updated_at"
        case comments, user, category
        case adsImages = "ads_images"
        case color
    }
}

// MARK: Datum convenience initializers and mutators

extension Datum {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Datum.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        nameObj: String?? = nil,
        datumDescription: String?? = nil,
        dateAnnonce: String?? = nil,
        colorID: String?? = nil,
        colorName: String?? = nil,
        longitude: String?? = nil,
        latitude: String?? = nil,
        address: String?? = nil,
        userID: String?? = nil,
        categoryID: String?? = nil,
        boostID: JSONNull?? = nil,
        reward: String?? = nil,
        state: String?? = nil,
        favoris: String?? = nil,
        enabled: String?? = nil,
        acceptedSignal: String?? = nil,
        itemFound: String?? = nil,
        deletedAt: JSONNull?? = nil,
        createdAt: String?? = nil,
        updatedAt: String?? = nil,
        comments: [Comment]?? = nil,
        user: User?? = nil,
        category: Category?? = nil,
        adsImages: [AdsImage]?? = nil,
        color: Color?? = nil
    ) -> Datum {
        return Datum(
            id: id ?? self.id,
            nameObj: nameObj ?? self.nameObj,
            datumDescription: datumDescription ?? self.datumDescription,
            dateAnnonce: dateAnnonce ?? self.dateAnnonce,
            colorID: colorID ?? self.colorID,
            colorName: colorName ?? self.colorName,
            longitude: longitude ?? self.longitude,
            latitude: latitude ?? self.latitude,
            address: address ?? self.address,
            userID: userID ?? self.userID,
            categoryID: categoryID ?? self.categoryID,
            boostID: boostID ?? self.boostID,
            reward: reward ?? self.reward,
            state: state ?? self.state,
            favoris: favoris ?? self.favoris,
            enabled: enabled ?? self.enabled,
            acceptedSignal: acceptedSignal ?? self.acceptedSignal,
            itemFound: itemFound ?? self.itemFound,
            deletedAt: deletedAt ?? self.deletedAt,
            createdAt: createdAt ?? self.createdAt,
            updatedAt: updatedAt ?? self.updatedAt,
            comments: comments ?? self.comments,
            user: user ?? self.user,
            category: category ?? self.category,
            adsImages: adsImages ?? self.adsImages,
            color: color ?? self.color
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseAdsImage { response in
//     if let adsImage = response.result.value {
//       ...
//     }
//   }

// MARK: - AdsImage
struct AdsImage: Codable {
    let id: Int?
    let image, adID: String?
    let deletedAt: JSONNull?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, image
        case adID
        case deletedAt
        case createdAt
        case updatedAt
    }
}

// MARK: AdsImage convenience initializers and mutators

extension AdsImage {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(AdsImage.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        image: String?? = nil,
        adID: String?? = nil,
        deletedAt: JSONNull?? = nil,
        createdAt: String?? = nil,
        updatedAt: String?? = nil
    ) -> AdsImage {
        return AdsImage(
            id: id ?? self.id,
            image: image ?? self.image,
            adID: adID ?? self.adID,
            deletedAt: deletedAt ?? self.deletedAt,
            createdAt: createdAt ?? self.createdAt,
            updatedAt: updatedAt ?? self.updatedAt
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseCategory { response in
//     if let category = response.result.value {
//       ...
//     }
//   }

// MARK: - Category
struct Category: Codable {
    let id: Int?
    let name, nameFr: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case nameFr
        case createdAt
        case updatedAt
    }
}

// MARK: Category convenience initializers and mutators

extension Category {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Category.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        name: String?? = nil,
        nameFr: String?? = nil,
        createdAt: String?? = nil,
        updatedAt: String?? = nil
    ) -> Category {
        return Category(
            id: id ?? self.id,
            name: name ?? self.name,
            nameFr: nameFr ?? self.nameFr,
            createdAt: createdAt ?? self.createdAt,
            updatedAt: updatedAt ?? self.updatedAt
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseColor { response in
//     if let color = response.result.value {
//       ...
//     }
//   }

// MARK: - Color
struct Color: Codable {
    let id: Int?
    let colorName: String?
}

// MARK: Color convenience initializers and mutators

extension Color {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Color.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        colorName: String?? = nil
    ) -> Color {
        return Color(
            id: id ?? self.id,
            colorName: colorName ?? self.colorName
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseComment { response in
//     if let comment = response.result.value {
//       ...
//     }
//   }

// MARK: - Comment
struct Comment: Codable {
    let id: Int?
    let content, userID, adID: String?
    let parentID, deletedAt: JSONNull?
    let createdAt, updatedAt: String?
    let replies: [JSONAny]?
    let user: User?

    enum CodingKeys: String, CodingKey {
        case id, content
        case userID = "user_id"
        case adID = "ad_id"
        case parentID
        case deletedAt
        case createdAt
        case updatedAt
        case replies, user
    }
}

// MARK: Comment convenience initializers and mutators

extension Comment {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Comment.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        content: String?? = nil,
        userID: String?? = nil,
        adID: String?? = nil,
        parentID: JSONNull?? = nil,
        deletedAt: JSONNull?? = nil,
        createdAt: String?? = nil,
        updatedAt: String?? = nil,
        replies: [JSONAny]?? = nil,
        user: User?? = nil
    ) -> Comment {
        return Comment(
            id: id ?? self.id,
            content: content ?? self.content,
            userID: userID ?? self.userID,
            adID: adID ?? self.adID,
            parentID: parentID ?? self.parentID,
            deletedAt: deletedAt ?? self.deletedAt,
            createdAt: createdAt ?? self.createdAt,
            updatedAt: updatedAt ?? self.updatedAt,
            replies: replies ?? self.replies,
            user: user ?? self.user
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseUser { response in
//     if let user = response.result.value {
//       ...
//     }
//   }

// MARK: - User
struct User: Codable {
    let id: Int?
    let firstname, lastname, email: String?
    let emailVerifiedAt: JSONNull?
    let phone, image, address, state: String?
    let role: Role?
    let deletedAt: JSONNull?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, firstname, lastname, email
        case emailVerifiedAt
        case phone, image, address, state, role
        case deletedAt
        case createdAt
        case updatedAt
    }
}

// MARK: User convenience initializers and mutators

extension User {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(User.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        firstname: String?? = nil,
        lastname: String?? = nil,
        email: String?? = nil,
        emailVerifiedAt: JSONNull?? = nil,
        phone: String?? = nil,
        image: String?? = nil,
        address: String?? = nil,
        state: String?? = nil,
        role: Role?? = nil,
        deletedAt: JSONNull?? = nil,
        createdAt: String?? = nil,
        updatedAt: String?? = nil
    ) -> User {
        return User(
            id: id ?? self.id,
            firstname: firstname ?? self.firstname,
            lastname: lastname ?? self.lastname,
            email: email ?? self.email,
            emailVerifiedAt: emailVerifiedAt ?? self.emailVerifiedAt,
            phone: phone ?? self.phone,
            image: image ?? self.image,
            address: address ?? self.address,
            state: state ?? self.state,
            role: role ?? self.role,
            deletedAt: deletedAt ?? self.deletedAt,
            createdAt: createdAt ?? self.createdAt,
            updatedAt: updatedAt ?? self.updatedAt
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

enum Role: String, Codable {
    case user = "user"
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    @discardableResult
    func responseWelcome(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Welcome>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
