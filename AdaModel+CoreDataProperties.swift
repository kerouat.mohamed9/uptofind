//
//  AdaModel+CoreDataProperties.swift
//  UpToFind
//
//  Created by Mohamed Kerouat on 2/25/20.
//  Copyright © 2020 Mohamed Kerouat. All rights reserved.
//
//

import Foundation
import CoreData


extension AdaModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AdaModel> {
        return NSFetchRequest<AdaModel>(entityName: "AdaModel")
    }

    @NSManaged public var name: String?

}
