//
//  AdModel+CoreDataProperties.swift
//  
//
//  Created by Mohamed Kerouat on 2/25/20.
//
//

import Foundation
import CoreData


extension AdModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AdModel> {
        return NSFetchRequest<AdModel>(entityName: "AdModel")
    }

    @NSManaged public var attribute: String?
    @NSManaged public var name: Bool

}
